package fastnet

import (
	"io"

	"codeberg.org/gruf/go-errors"
)

// ErrBodyClosed is returned if a read is attempted on a closed BodyReader
var ErrBodyClosed = errors.New("fastnet: read on closed body")

// ErrNegativeBody is returned if a read is attempted on a BodyReader with negative body size
var ErrNegativeBody = errors.New("fastnet: negative body size")

// Body provides a means of reading a fixed-size request body
type Body struct {
	cls bool          // closed
	rem int64         // remaining bytes
	rc  io.ReadCloser // underlying body
	err error         // last set error
}

// NewBody returns a new Body from io.ReadCloser and for set size
func NewBody(rc io.ReadCloser, size int64) Body {
	var err error
	switch {
	case size < 0:
		err = ErrNegativeBody
	case size == 0:
		err = io.EOF
	default:
		// no starting error
	}
	return Body{
		rem: size,
		rc:  rc,
		err: err,
	}
}

func (b *Body) Read(p []byte) (int, error) {
	if b.cls {
		// Read on closed body
		return 0, ErrBodyClosed
	}

	if b.rem < 1 {
		// Hit max bytes, return last err
		return 0, b.err
	}

	if int64(len(p)) > b.rem {
		// Supplied larger slice than we
		// have remaining, reslice
		p = p[:b.rem]
	}

	var n int

	// Read next bytes from reader
	n, b.err = b.rc.Read(p)
	b.rem -= int64(n)

	switch {
	case (b.rem < 1):
		// Ran out of bytes to read, either
		// set EOF or leave provided
		if b.err == nil {
			b.err = io.EOF
		}

	case (b.err != nil):
		// Check for EOF, we drop remaining
		// bytes to zero if so
		if b.err == io.EOF {
			b.rem = 0
		}
	}

	return n, b.err
}

func (b *Body) Close() error {
	if !b.cls {
		b.cls = true
	}
	return b.rc.Close()
}
